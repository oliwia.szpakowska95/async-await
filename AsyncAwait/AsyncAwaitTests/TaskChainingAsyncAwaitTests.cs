﻿using NUnit.Framework;
#pragma warning disable CS8625
#pragma warning disable S4487
#pragma warning disable IDE0052

namespace TaskChainingTests
{
    /// <summary>
    /// This class tests logic of TaskChainingAsyncAwait.
    /// </summary>
    [TestFixture]
    public class TaskChainingAndContinuationTest
    {
        AsyncAwaitLibrary.TaskChainingAsyncAwait tasks;
        Random randomNumber;

        [SetUp]
        public void SetUp()
        {
            tasks = new AsyncAwaitLibrary.TaskChainingAsyncAwait();
            randomNumber = new Random();
        }

        [Test]
        public void ArrayMultiplication_ForInvalidArgument_ThrowsArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.MultiplyArray(null, 3));
        }

        [Test]
        public void InitializationWithRandomNumber_ForInvalidArgument__ThrowsArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.InitializeArrayWithRandom(null));
        }

        [Test]
        public void SortArrayByAscending_ForInvalidArgument__ThrowsArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.SortArrayByAscending(null));
        }

        [Test]
        public void CalculateAverage_ForInvalidArgument_ThrowsArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await tasks.CalculateAverage(null));
        }

        [Test]
        public void CreateNewArray_ForInvalidArgument_ThrowsArgumentException()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateNewArray(int.MinValue));
            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateNewArray(-1));
            Assert.ThrowsAsync<ArgumentException>(async () => await tasks.CreateNewArray(0));
        }

        [Test]
        public async Task InitializeArrayWithRandom_InitializationTest()
        {
            var array = new int[10];
            var initializedArray = await tasks.InitializeArrayWithRandom(array);
            bool isAnyValueZero = false;

            for (int i = 0; i < initializedArray.Length; i++)
            {
                if (initializedArray[i] != 0)
                {
                    isAnyValueZero = false;
                }
            }
            Assert.That(isAnyValueZero, Is.False, "Unlucky test with random value 0 or values not initializated");
        }

        [Test]
        public async Task MultiplyArray_PositiveArrayMultiplicationTest()
        {
            int[] array = new int[5];
            array[0] = 1;
            array[1] = 2;
            array[2] = 3;
            array[3] = 4;
            array[4] = 5;
            long sum = 0;

            var multiplier = 2;
            var resultArray = await tasks.MultiplyArray(array, multiplier);
            foreach (var result in resultArray)
            {
                sum += result;
            }

            Assert.That(sum, Is.EqualTo(30));
        }

        [Test]
        public async Task MultiplyArray_ArrayMultiplicationForNegativeValuesTest()
        {
            int[] array = new int[5];
            array[0] = -1;
            array[1] = -2;
            array[2] = -3;
            array[3] = -4;
            array[4] = -5;
            long sum = 0;

            var multiplier = 2;
            var resultArray = await tasks.MultiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                sum += result;
            }

            Assert.That(sum, Is.EqualTo(-30));
        }

        [Test]
        public async Task MultiplyArray_ArrayMultiplicationForZeroTest()
        {
            int[] array = new int[5];
            array[0] = -1;
            array[1] = -2;
            array[2] = -3;
            array[3] = -4;
            array[4] = -5;
            long sum = 0;

            var multiplier = 0;
            var resultArray = await tasks.MultiplyArray(array, multiplier);

            foreach (var result in resultArray)
            {
                sum += result;
            }
            Assert.That(sum, Is.EqualTo(0));
        }

        [Test]
        public async Task SortArrayByAscending_IsArraySorted()
        {
            var array = new long[6];
            array[0] = int.MinValue;
            array[1] = int.MaxValue;
            array[2] = 0;
            array[3] = -50;
            array[4] = -1;
            array[5] = 220;

            var expectedArrayAfterSorting = new long[6] { int.MinValue, -50, -1, 0, 220, int.MaxValue };
            var sortedArrayByTask = await tasks.SortArrayByAscending(array);
            
            Assert.That(sortedArrayByTask, Is.EqualTo(expectedArrayAfterSorting));
        }

        [Test]
        public async Task CalculateAverage_IsAverageCorrect()
        {
            var array = new long[5];
            array[0] = 10;
            array[1] = 10;
            array[2] = 10;
            array[3] = 10;
            array[4] = 10;

            var average = await tasks.CalculateAverage(array);
            
            Assert.That(average, Is.EqualTo(10));
        }
    }
}