﻿#pragma warning disable CA1822
namespace AsyncAwaitLibrary
{
    /// <summary>
    /// Represents library class for four async tasks:
    /// Task 1: creates an array of 10 random integers.
    /// Task 2: multiplies the array by a randomly generated number.
    /// Task 3: sorts the array by ascending.
    /// Task 4: calculates the average value.
    /// </summary>
    public class TaskChainingAsyncAwait
    {
        /// <summary>
        /// This method generates new array.
        /// </summary>
        /// <param name="size">Array size.</param>
        /// <exception cref="ArgumentException">Throw when size is less than 1.</exception>
        public async Task<int[]> CreateNewArray(int size = 10)
        {
            return await Task.Run(() =>
            {
                if (size < 1)
                {
                    throw new ArgumentException("Array size cannot be smaller than 1.");
                }
                return new int[size];
            });
        }

        /// <summary>
        /// This method fill an array by random numbers.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <exception cref="ArgumentNullException">Throw by CheckingInputArray when array is null.</exception>
        public async Task<int[]> InitializeArrayWithRandom(int[] array)
        {
            return await Task.Run(() =>
            {
                CheckingInputArray<int[]>(array);
                var randomNumber = new Random();
                for (var i = 0; i < array.Length; i++)
                {
                    array[i] = randomNumber.Next();
                }
                return array;
            });
        }

        /// <summary>
        /// This method multiplies the array by a randomly generated number.
        /// </summary>
        /// <param name="multiplier">Multiplier number.</param>
        /// <param name="array">Array of integers.</param>
        /// <exception cref="ArgumentNullException">Throw by CheckingInputArray when array is null.</exception>
        public async Task<long[]> MultiplyArray(int[] array, int multiplier)
        {
            return await Task.Run(() =>
            {
                CheckingInputArray<int[]>(array);
                var resultArray = new long[array.Length];
                for (var i = 0; i < array.Length; i++)
                {
                    resultArray[i] = array[i] * (long)multiplier;
                }
                return resultArray;
            });
        }

        /// <summary>
        /// This method sorts the array by ascending.
        /// </summary>
        /// <param name="array">Array of longs.</param>
        /// <exception cref="ArgumentNullException">Throw by CheckingInputArray when array is null.</exception>
        public async Task<long[]> SortArrayByAscending(long[] array)
        {
            return await Task.Run(() =>
            {
                CheckingInputArray<long[]>(array);
                var sortedArray = new long[array.Length];
                Array.Copy(array, 0, sortedArray, 0, array.Length);
                Array.Sort(sortedArray, 0, sortedArray.Length);
                return sortedArray;
            });
        }

        /// <summary>
        /// This method calculates the average value.
        /// </summary>
        /// <param name="array">Array of longs.</param>
        /// <exception cref="ArgumentNullException">Throw by CheckingInputArray when array is null.</exception>
        public async Task<decimal> CalculateAverage(long[] array)
        {
            return await Task.Run(() =>
            {
                CheckingInputArray<long[]>(array);
                var average = Convert.ToDecimal(array[0]);
                for (var i = 1; i < array.Length; i++)
                {
                    average = (average + array[i]) / 2;
                }
                return average;
            });
        }

        /// <summary>
        /// This method checks is input array null.
        /// </summary>
        /// <param name="array">Array of integers.</param>
        /// <exception cref="ArgumentNullException">Throw when array is null.</exception>
        private void CheckingInputArray<T>(T array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
        }
    }
}