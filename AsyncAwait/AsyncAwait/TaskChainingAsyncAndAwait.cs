﻿using System;
#pragma warning disable S1118

namespace AsyncAwait
{
    public class TaskChainingAsyncAndAwait
    {
        static async Task Main()
        {
            AsyncAwaitLibrary.TaskChainingAsyncAwait tasks = new();
            var randomNumber = new Random();

            var array = await tasks.CreateNewArray(10);
            Console.WriteLine("Array created.");
            var initializedArray = await tasks.InitializeArrayWithRandom(array);
            PrintArray<int>("\nArray initialized by random integers:", initializedArray);
            var multipliedArray = await tasks.MultiplyArray(initializedArray, randomNumber.Next(1000));
            PrintArray<long>("\nArray multiplied by random number:", multipliedArray);
            var sortedArray = await tasks.SortArrayByAscending(multipliedArray);
            PrintArray<long>("\nArray sorted by ascending:", sortedArray);
            var average = await tasks.CalculateAverage(sortedArray);

            Console.WriteLine($"\nAverage value: {average}");
            Console.ReadLine();
        }

        private static void PrintArray<T>(string message, T[] array) where T : struct
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Console.WriteLine(message);
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].ToString());
            }
        }
    }
} 